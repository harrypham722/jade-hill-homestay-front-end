import React, { useState, useContext, useEffect } from "react";
import ReactDom from "react-dom";
import bg_main from "../../../public/images/bg_main.jpg";
import { Image, Container, Row, Col, Collapse, Button } from "react-bootstrap";
import hs1 from "../../../public/images/hs1.jpg";
import hs2 from "../../../public/images/hs2.jpg";
import hs3 from "../../../public/images/hs3.jpg";
import "../ScreenCSS/HomeStayItemDetail.css";
import { HomestayContext } from "../../Context/HomestayContext";
import AddNewModal from "../Component/AddNewModal";
import { withRouter } from "react-router-dom";
import HomestayAPI from "../../API/HomestayAPI/HomestayAPI";

const HomeStayItemDetail = ({ history, match }) => {
  const [modal, setModal] = useState(false);
  const [homestayItem, setHomestayItem] = useState(undefined);
  const { deleteHomestay } = useContext(HomestayContext);
  const { getParticularHomestay } = HomestayAPI();
  const showModal = () => {
    setModal(true);
  };
  const closeModal = () => {
    setModal(false);
  };

  useEffect(() => {
    const fetchData = async () => {
      const result = await getParticularHomestay(match.params.homestayId);
      setHomestayItem(result);
    };
    fetchData();
  }, []);

  if (homestayItem === undefined) {
    return <h1>Loading...</h1>;
  }
  const updateHandle = () => {
    history.push(`/homestays/${match.params.homestayId}/update`);
  };
  return (
    <div
      className="flex-grow-1"
      style={{
        backgroundColor: "white",
        color: "#242424",
        paddingBottom: "10px",
        //minHeight: "100%",
      }}
    >
      <div className="hs-item-container">
        <div className="hs-item-info">
          <h1 style={{ marginBottom: "10px", paddingTop: "10px" }}>
            {homestayItem.name}
          </h1>

          <div className="col-10 offset-1">
            <p className="text-start">Address: {homestayItem.address}</p>
            <p className="text-start">Phone: {homestayItem.phone}</p>
            <p className="text-start">
              Description: {homestayItem.description}
            </p>
          </div>
          <div>
            <Button
              className="m-3 border-0 bg-secondary"
              onClick={updateHandle}
            >
              Update
            </Button>
            <Button className="m-3 border-0 bg-secondary" onClick={showModal}>
              Delete
            </Button>
          </div>
        </div>
      </div>

      <Container>
        <Row>
          <Col xs={6} md={4}>
            <Image style={{ marginBottom: "10px" }} src={hs1} thumbnail />
          </Col>
          <Col xs={6} md={4}>
            <Image style={{ marginBottom: "10px" }} src={hs2} thumbnail />
          </Col>
          <Col xs={6} md={4}>
            <Image style={{ marginBottom: "10px" }} src={hs3} thumbnail />
          </Col>
          <Col xs={6} md={4}>
            <Image style={{ marginBottom: "10px" }} src={hs1} thumbnail />
          </Col>
          <Col xs={6} md={4}>
            <Image style={{ marginBottom: "10px" }} src={hs2} thumbnail />
          </Col>
          <Col xs={6} md={4}>
            <Image style={{ marginBottom: "10px" }} src={hs3} thumbnail />
          </Col>
          <Col xs={6} md={4}>
            <Image style={{ marginBottom: "10px" }} src={hs1} thumbnail />
          </Col>
          <Col xs={6} md={4}>
            <Image style={{ marginBottom: "10px" }} src={hs2} thumbnail />
          </Col>
          <Col xs={6} md={4}>
            <Image style={{ marginBottom: "10px" }} src={hs3} thumbnail />
          </Col>
        </Row>
      </Container>

      <AddNewModal
        title="Are you sure?"
        show={modal}
        close={closeModal}
        action={() => deleteHomestay(homestayItem._id)}
        input={false}
        actionName="Delete"
      />
    </div>
  );
};

export default HomeStayItemDetail;
