import React, { useReducer, useState } from "react";

export const LoginContext = React.createContext(null);

export default ({ children }) => {
  const [token, setToken] = useState(localStorage.getItem('token'));
  return (
    <LoginContext.Provider value={{ token, setToken }}>
      {children}
    </LoginContext.Provider>
  );
};
