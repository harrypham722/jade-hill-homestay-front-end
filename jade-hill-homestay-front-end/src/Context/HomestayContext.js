import React, { useReducer, useEffect, useContext } from "react";
import HomestayAPI from "../API/HomestayAPI/HomestayAPI";
import { LoginContext } from "./LoginContext";

const reducer = (state, action) => {
  switch (action.type) {
    case "GET_ALL_HOMESTAY":
      return [...action.value];
    // case "ADD_HOMESTAY":
    //   return [
    //     ...state,
    //     { ...action.value },
    //   ];
    case "DELETE_HOMESTAY":
      return state.filter((item) => item.id !== action.value);
    case "UPDATE_HOMESTAY":
      let newState = [...state];
      const itemIndex = newState.findIndex(
        (item) => item.id === action.value.id
      );
      console.log(newState[itemIndex]);
      newState[itemIndex].name = action.value.name;
      newState[itemIndex].address = action.value.address;
      newState[itemIndex].description = action.value.description;
      newState[itemIndex].cityid = action.value.cityid;
      newState[itemIndex].images = action.value.images;
      return newState;
    default:
      return state;
  }
};

export const HomestayContext = React.createContext(null);

export default ({ children }) => {
  const { getAllHomestay, postNewHomestay, putHomestayById, deleteHomestayById } = HomestayAPI();
  const { token } = useContext(LoginContext);

  useEffect(() => {
    const fetchData = async () => {
      const result = await getAllHomestay();
      getHomestay(result);
    };
    if (token !== null) {
      fetchData();
    }
  }, [token]);

  const [homestayList, dispatch] = useReducer(reducer, []);

  const getHomestay = (value) => {
    dispatch({ type: "GET_ALL_HOMESTAY", value: value });
  };

  const addHomestay = async (value) => {
    await postNewHomestay(value);
    const result = await getAllHomestay();
    getHomestay(result);
  };

  const deleteHomestay = async (homestayID) => {
    await deleteHomestayById(homestayID);
    const result = await getAllHomestay();
    getHomestay(result);
  };

  const updateHomestay = async (value, homestayId) => {
    await putHomestayById(value, homestayId);
    const result = await getAllHomestay();
    getHomestay(result);
  };

  return (
    <HomestayContext.Provider
      value={{ homestayList, addHomestay, deleteHomestay, updateHomestay }}
    >
      {children}
    </HomestayContext.Provider>
  );
};
