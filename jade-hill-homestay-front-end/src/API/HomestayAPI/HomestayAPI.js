import React, { useContext } from "react";
import axios from "axios";
import { LoginContext } from "../../Context/LoginContext";

//const url = process.env.REACT_APP_SERVER;
const url = "http://0925-2402-800-61ee-14b9-80b7-bb88-3ce6-b591.ngrok.io";

export default () => {
  console.log(url);
  const { token } = useContext(LoginContext);
  const getAllHomestay = async () => {
    const response = await axios({
      method: "get",
      url: `${url}/homestays`,
      responseType: "json",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    const homestayList = await response.data;
    return homestayList;
  };

  const getParticularHomestay = async (homestayId) => {
    const response = await axios({
      method: "get",
      url: `${url}/homestays/${homestayId}`,
      responseType: "json",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    const homestayItem = await response.data;
    return homestayItem;
  };

  const postNewHomestay = async (newHomestay) => {
    console.log(newHomestay);
    const request = await axios({
      method: "post",
      url: `${url}/homestays/create`,
      data: newHomestay,
      responseType: "json",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });

    const data = await request.data;
    console.log(data);
    return data;
  };

  const deleteHomestayById = async (homestayID) => {
    const response = await axios({
      method: "delete",
      url: `${url}/homestays/${homestayID}`,
      responseType: "json",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });

    const data = response.data;
    return data;
  };

  const putHomestayById = async (updateInfo, homestayId) => {
    const response = await axios({
      method: "post",
      url: `${url}/homestays/${homestayId}/update`,
      data: { data: updateInfo },
      responseType: "json",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    const data = await response.data;
    return data;
  };
  return {
    getAllHomestay,
    getParticularHomestay,
    deleteHomestayById,
    putHomestayById,
    postNewHomestay,
  };
};
