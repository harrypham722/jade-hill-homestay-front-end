import React, { useReducer } from "react";
import ReactDom from "react-dom";
import "../ScreenCSS/AdminHomeScreen.css";
import { Link, withRouter } from "react-router-dom";
import { Button } from "react-bootstrap";
import { FaChevronCircleDown, FaChevronCircleUp } from "react-icons/fa";

const reducer = (state, action) => {
  switch (action.type) {
    case "ACCOUNT_MENU":
      return { ...state, accountMenu: !state.accountMenu };
    case "HOMESTAY_MENU":
      return { ...state, homestayMenu: !state.homestayMenu };
    default:
      return state;
  }
};

const AdminHomeScreen = ({ onSetAuth, history }) => {
  const initState = { accountMenu: false, homestayMenu: false };
  const [menuState, dispatch] = useReducer(reducer, initState);

  const handleHomestayMenu = () => {
    dispatch({ type: "HOMESTAY_MENU" });
  };

  const handleAccountMenu = () => {
    dispatch({ type: "ACCOUNT_MENU" });
  };

  return (
    <nav
      style={{ height: "100vh" }}
      className="d-flex flex-column align-items-center justify-content-between"
    >
      <ul className="col-10 d-flex flex-column align-items-start justify-content-start nav-links">
        <li className="nav-item-container d-flex flex-column align-items-start">
          <a
            href="#"
            onClick={handleHomestayMenu}
            className="nav-item "
            style={{ fontSize: "20px", marginRight: "5px" }}
          >
            <div className="d-flex align-items-center">
              <p style={{ margin: "5px" }}>Homestay</p>
              {menuState.homestayMenu ? (
                <FaChevronCircleUp size={14} />
              ) : (
                <FaChevronCircleDown size={14} />
              )}
            </div>
          </a>
          <div
            className={
              menuState.homestayMenu
                ? "d-flex flex-column align-items-start ms-4 "
                : "d-none"
            }
          >
            <Link className="nav-item" to="/homestays/create">
              Create Homestay
            </Link>
            <Link className="nav-item" to="/homestays">
              List Homestay
            </Link>
          </div>
        </li>

        <li className="nav-item-container d-flex flex-column align-items-start">
          <a
            href="#"
            onClick={handleAccountMenu}
            className="nav-item"
            style={{ fontSize: "20px", marginRight: "5px" }}
          >
            <div className="d-flex align-items-center">
              <p style={{ margin: "5px" }}>Account</p>
              {menuState.homestayMenu ? (
                <FaChevronCircleUp size={14} />
              ) : (
                <FaChevronCircleDown size={14} />
              )}
            </div>
          </a>
          <div
            className={
              menuState.accountMenu
                ? "d-flex flex-column align-items-start ms-4 "
                : "d-none"
            }
          >
            <Link className="nav-item" to="/users/store">
              Create Account
            </Link>
            <Link className="nav-item" to="/users">
              List Account
            </Link>
          </div>
        </li>
      </ul>
      <Button
        className="logout-btn"
        onClick={() => {
          localStorage.removeItem("loggedin");
          localStorage.removeItem('token');
          onSetAuth(false);
          history.push("/");
        }}
      >
        Log out
      </Button>
    </nav>
  );
};

export default withRouter(AdminHomeScreen);
