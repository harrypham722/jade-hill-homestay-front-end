import "./App.css";
import LoginScreen from "./Screen/ScreenJS/LoginScreen";
import AdminHomeScreen from "./Screen/ScreenJS/AdminHomeScreen";
import CreateAccountScreen from "./Screen/ScreenJS/CreateAccountScreen";
import CreateHomestayScreen from "./Screen/ScreenJS/CreateHometayScreen";
import ListHomeStayScreen from "./Screen/ScreenJS/ListHomeStayScreen";
import HomeStayItemDetail from "./Screen/ScreenJS/HomeStayItemDetail";
import HomeScreen from "./Screen/ScreenJS/HomeScreen";
import UpdateHomestayScreen from "./Screen/ScreenJS/UpdateHomestayScreen";
import ListAccountScreen from "./Screen/ScreenJS/ListAccountScreen";
import Header from "./Screen/Component/Header";
import AccountAPI from "./API/AccountAPI/AccountAPI";
import { LoginContext } from "./Context/LoginContext";
import React, { useState, useContext } from "react";

import { Route, Switch } from "react-router-dom";

function App() {
  const [auth, setAuth] = useState(localStorage.getItem("loggedin"));
  const { setToken } = useContext(LoginContext);
  const { loginAuthenticate } = AccountAPI();

  const authenticate = async (value) => {
    console.log(value);
    const response = await loginAuthenticate(value);
    if (response !== "") {
      setAuth(true);
      localStorage.setItem("token", response);
      setToken(localStorage.getItem("token"));
      localStorage.setItem("loggedin", "true");
    }
  };

  return auth ? (
    <div className="App" style={{ minHeight: "100%" }}>
      <div
        className="container-fluid d-flex m-0 p-0"
        style={{ minHeight: "100%" }}
      >
        <div
          className="col-2 h-100"
          style={{
            position: "sticky",
            zIndex: "999",
            top: "0px",
          }}
        >
          <AdminHomeScreen onSetAuth={setAuth} />
        </div>
        <div className="col-10" style={{minHeight: "100%"}}>
          <Header />
          <Switch>
            <Route path="/" component={HomeScreen} exact />
            <Route path="/homestays" component={ListHomeStayScreen} exact />
            <Route path="/users/store" component={CreateAccountScreen} exact />
            <Route
              path="/homestays/create"
              component={CreateHomestayScreen}
              exact
            />
            <Route
              path="/homestays/:homestayId"
              component={HomeStayItemDetail}
              exact
            />
            <Route
              path="/homestays/:homestayId/update"
              component={UpdateHomestayScreen}
              exact
            />
            <Route path="/users" component={ListAccountScreen} exact />
          </Switch>
        </div>
      </div>
    </div>
  ) : (
    <div className="App" style={{ height: "100%" }}>
      <LoginScreen onSetAuth={authenticate} />
    </div>
  );
}

export default App;
