import React, { useEffect, useContext } from "react";
import ReactDom from "react-dom";
import { Table } from "react-bootstrap";
import { AccountContext } from "../../Context/AccountContext";
import { Link } from "react-router-dom";
import "../ScreenCSS/ListAccountScreen.css"

const headingArray = [
  "#",
  "First name",
  "Last name",
  "Email",
  "Phone number",
  "Role",
  "Action",
];

const ListAccountScreen = () => {
  // const fetchAPI = async () => {
  //   const data = await fetch("https://jsonplaceholder.typicode.com/todos");
  //   const items = await data.json();
  //   setItems(items);
  // };
  const { accountList } = useContext(AccountContext);
//   const { token } = useContext(LoginContext);

//   useEffect(async () => {
//     const result = await getAllAccount(token);
//     console.log("here", result);
//     getAccount(result);
//   }, []);

  return (
    <div
      className="flex-grow-1 d-flex flex-column list-acc-container"
      //style={{minHeight: "100%"}}
    >
      <h1 className="list-acc-title">List Account</h1>
      <Table striped hover borderless responsive variant="light">
        <thead>
          <tr style={{ textAlign: "left" }}>
            {headingArray.map((item, index) => (
              <th
                style={{ backgroundColor: "#333333", color: "white" }}
                key={index}
              >
                {item}
              </th>
            ))}
          </tr>
        </thead>
        <tbody>
          {accountList.map((item, index) => (
            <tr style={{ textAlign: "left" }} key={index}>
              <td>{index + 1}</td>
              <td>{item.first_name}</td>
              <td>{item.last_name}</td>
              <td>{item.email}</td>
              <td>{item.phone_num}</td>
              <td>{item.role}</td>
              <td>
                <Link
                  className="text-decoration-none"
                  //   to={`/list-account/${item.id}`}
                  to="/"
                >
                  Click to see detail
                </Link>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </div>
  );
};

export default ListAccountScreen;
