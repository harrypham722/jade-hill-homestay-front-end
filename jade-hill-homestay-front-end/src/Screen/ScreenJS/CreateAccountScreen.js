import React, { useReducer, useContext } from "react";
import ReactDom from "react-dom";
import bg_main from "../../../public/images/bg_main.jpg";
import "../ScreenCSS/CreateAccountScreen.css";
import { Form, Button, ListGroup } from "react-bootstrap";
import { AccountContext } from "../../Context/AccountContext";
import { withRouter } from "react-router-dom";

const reducer = (state, action) => {
  switch (action.type) {
    case "CHANGED_FIRST_NAME":
      return { ...state, first_name: action.value };
    case "CHANGED_LAST_NAME":
      return { ...state, last_name: action.value };
    case "CHANGED_EMAIL":
      return { ...state, email: action.value };
    case "CHANGED_PHONE_NUM":
      return { ...state, phone_num: action.value };
    case "CHANGED_PASSWORD":
      return { ...state, password: action.value };
    case "CHANGED_ROLE":
      return { ...state, role: action.value };
    case "MESSAGE":
      return { ...state, message: action.value };
    default:
      return state;
  }
};

const roles = ["Manager", "Staff"];

const init = {
  first_name: "",
  last_name: "",
  email: "",
  phone_num: "",
  password: "",
  role: roles[0],
  message: "",
};

const CreateAccountScreen = ({ history }) => {
  const { addAccount, accountList } = useContext(AccountContext);
  const [state, dispatch] = useReducer(reducer, init);

  const changeFirstName = (value) => {
    dispatch({ type: "CHANGED_FIRST_NAME", value: value });
  };

  const changeLastName = (value) => {
    dispatch({ type: "CHANGED_LAST_NAME", value: value });
  };

  const changeEmail = (value) => {
    dispatch({ type: "CHANGED_EMAIL", value: value });
  };

  const changePhoneNum = (value) => {
    dispatch({ type: "CHANGED_PHONE_NUM", value: value });
  };

  const changePassword = (value) => {
    dispatch({ type: "CHANGED_PASSWORD", value: value });
  };

  const changeRole = (value) => {
    dispatch({ type: "CHANGED_ROLE", value: value });
  };

  const changeMessage = (value) => {
    dispatch({ type: "MESSAGE", value: value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    // const accountIndex = accountList.findIndex(
    //   (item) => item.email === state.email
    // );
    // if (accountIndex >= 0) {
    //   changeMessage("Account existed!");
    //   return;
    // }
    const { first_name, last_name, email, phone_num, role, password } = state;
    addAccount({ first_name, last_name, email, phone_num, role, password });
    history.push("/users");
    changeFirstName("");
    changeLastName("");
    changeEmail("");
    changePassword("");
    changePhoneNum("");
    changeRole(role[0]);
    changeMessage("Account created!");
  };

  return (
    <div
      className="flex-grow-1 create-acc-container"
      style={{ backgroundColor: "white" }}
    >
      <div style={{ width: "100%" }}>
        <h1 className="create-acc-title">Create Account</h1>
        <div className="col-4 offset-4 create-acc-signup-form">
          <Form onSubmit={handleSubmit}>
            <Form.Control
              className="mb-3"
              type="text"
              placeholder="First name"
              required
              onChange={(e) => {
                changeFirstName(e.target.value);
                changeMessage("");
              }}
              value={state.first_name}
            />
            <Form.Control
              className="mb-3"
              type="text"
              placeholder="Last name"
              required
              value={state.last_name}
              onChange={(e) => {
                changeLastName(e.target.value);
                changeMessage("");
              }}
            />
            <Form.Control
              className="mb-3"
              type="email"
              placeholder="Email"
              required
              value={state.email}
              onChange={(e) => {
                changeEmail(e.target.value);
                changeMessage("");
              }}
            />
            <Form.Control
              className="mb-3"
              type="text"
              placeholder="Phone number"
              required
              value={state.phone_num}
              onChange={(e) => {
                changePhoneNum(e.target.value);
                changeMessage("");
              }}
            />
            <Form.Control
              className="mb-3"
              type="password"
              placeholder="Password"
              required
              value={state.password}
              onChange={(e) => {
                changePassword(e.target.value);
                changeMessage("");
              }}
            />
            <div className="mb-3">
              <Form.Select
                required
                onChange={(e) => {
                  changeRole(e.target.value);
                }}
                value={state.role}
              >
                {roles.map((item, index) => (
                  <option key={index} value={item}>
                    {item}
                  </option>
                ))}
              </Form.Select>
            </div>
            <Button type="submit" className="bg-secondary border-0">
              Create account
            </Button>
            <p className="create-acc-invalid-inform">{state.message}</p>
          </Form>
        </div>
      </div>
    </div>
  );
};

export default withRouter(CreateAccountScreen);
