import React, { useContext } from "react";
import axios from "axios";
import { LoginContext } from "../../Context/LoginContext";

//const url = process.env.REACT_APP_SERVER;
const url = "http://0925-2402-800-61ee-14b9-80b7-bb88-3ce6-b591.ngrok.io";

export default () => {
  const { token } = useContext(LoginContext);
 
  const loginAuthenticate = async (loginInfo) => {
    const response = await axios({
      method: "post",
      url: `${url}/login`,
      responseType: "json",
      data: loginInfo,
    });
    const message = await response.data;
    return message;
  };

  const getAllAccount = async () => {
    const response = await axios({
      method: "get",
      url: `${url}/users`,
      responseType: "json",
      headers: { Authorization: `Bearer ${token}` },
    });
    const result = await response.data;
    return result;
  };

  const postNewAccount = async (accountInfo) => {
    const response = await axios({
      method: "post",
      url: `${url}/users/store`,
      responseType: "json",
      data: accountInfo,
      headers: { Authorization: `Bearer ${token}` },
    });
    const result = await response.data;
    return result;
  };

  return { loginAuthenticate, postNewAccount, getAllAccount };
};
