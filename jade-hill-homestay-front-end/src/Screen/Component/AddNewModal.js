import React, { useState } from "react";
import ReactDom from "react-dom";
import { Modal, Form, Button } from "react-bootstrap";
import { withRouter } from "react-router-dom";

const AddNewModal = ({
  title,
  show,
  close,
  action,
  input,
  actionName,
  history,
}) => {
  const [newCity, setNewCity] = useState("");
  const handleSubmit = (event) => {
    event.preventDefault();
    if (input === true) {
      action(newCity);
    } else {
      action();
      history.push("/homestays");
    }
  };
  return (
    <Modal show={show} onHide={close}>
      <Modal.Header closeButton>
        <Modal.Title>{title}</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form onSubmit={handleSubmit}>
          {input && (
            <Form.FloatingLabel label="New city">
              <Form.Control
                className="mb-3"
                required
                placeholder="newcity"
                onChange={(e) => setNewCity(e.target.value)}
              />
            </Form.FloatingLabel>
          )}
          <Button type="submit" onClick={close}>
            {actionName}
          </Button>
        </Form>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={close}>
          Close
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default withRouter(AddNewModal);
