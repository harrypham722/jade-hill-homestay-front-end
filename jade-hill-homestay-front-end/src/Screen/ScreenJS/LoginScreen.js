import React, { useState, useContext } from "react";
import ReactDom from "react-dom";
import { Form, Button } from "react-bootstrap";
import "../ScreenCSS/LoginScreen.css";
import bg_login from "../../../public/images/bg_login.jpg";
import { FaHome } from "react-icons/fa";
import { Link } from "react-router-dom";
import { AccountContext } from "../../Context/AccountContext";

const LoginScreen = ({ onSetAuth, token }) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [message, setMessage] = useState("");
  const { accountList } = useContext(AccountContext);

  const handleSubmit = (event) => {
    event.preventDefault();
    onSetAuth({ email, password});
    setMessage("");
  };

  return (
    <div
      style={{
        background: "#ece5df",
      }}
      className="login-root flex-grow-1"
    >
      <div className="login-title">
        <FaHome size="50" />
        <h1 className="login-title-text">LOGIN</h1>
      </div>
      <div className="login-form">
        <Form onSubmit={handleSubmit}>
          <Form.Control
            className="mb-3"
            type="email"
            placeholder="Email"
            onChange={(info) => {
              setEmail(info.target.value);
              setMessage("");
            }}
            required
          />
          <Form.Control
            className="mb-3"
            type="password"
            placeholder="Password"
            onChange={(info) => {
              setPassword(info.target.value);
              setMessage("");
            }}
            required
          />
          <p>{message}</p>
          <Button className="login-btn" type="submit">
            GO
          </Button>
        </Form>
        <Link to="/">Forgot password?</Link>
      </div>
    </div>
  );
};

export default LoginScreen;
