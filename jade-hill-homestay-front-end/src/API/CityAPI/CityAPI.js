import React, { useContext } from "react";
import axios from "axios";
import { LoginContext } from "../../Context/LoginContext";

//const url = process.env.REACT_APP_SERVER;
const url = "http://0925-2402-800-61ee-14b9-80b7-bb88-3ce6-b591.ngrok.io";

export default () => {
  const { token } = useContext(LoginContext);
  const getAllCity = async () => {
    const response = await axios({
      method: "get",
      url: `${url}/cities`,
      responseType: "json",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    const homestayList = await response.data;
    return homestayList;
  };

  const postNewCity = async (newCity) => {
    const response = await axios({
      method: "post",
      url: `${url}/cities/create`,
      responseType: "json",
      headers: {
        Authorization: `Bearer ${token}`,
      },
      data: newCity,
    });
    const data = await response.data;
    return data;
  };

  return {
    getAllCity,
    postNewCity,
  };
};
