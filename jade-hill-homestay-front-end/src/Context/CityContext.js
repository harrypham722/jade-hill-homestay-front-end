import React, { useReducer, useEffect, useContext } from "react";
import CityAPI from "../API/CityAPI/CityAPI";
import { LoginContext } from "./LoginContext";

const reducer = (state, action) => {
  switch (action.type) {
    case "GET_CITIES":
      const cities = action.value.map(item => item.name);
      return cities;
    case "ADD_CITY":
      return [...state, action.value];
    default:
      return state;
  }
};

export const CityContext = React.createContext(null);

export default ({ children }) => {
  const { getAllCity } = CityAPI();
  const { token } = useContext(LoginContext);

  useEffect(() => {
    const fetchData = async () => {
      const result = await getAllCity();
      getCities(result);
    };
    if (token !== null) {
      fetchData();
    }
  }, [token]);

  const getCities = (value) => {
    dispatch({ type: "GET_CITIES", value: value });
  };

  const addCity = (value) => {
    dispatch({ type: "ADD_CITY", value: value });
  };

  const [cityID, dispatch] = useReducer(reducer, []);
  return (
    <CityContext.Provider value={{ cityID, addCity }}>
      {children}
    </CityContext.Provider>
  );
};
