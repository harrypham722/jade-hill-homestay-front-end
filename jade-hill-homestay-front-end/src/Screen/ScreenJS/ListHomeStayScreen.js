import React, { useState, useEffect, useContext } from "react";
import ReactDom from "react-dom";
import bg_main from "../../../public/images/bg_main.jpg";
import { Table } from "react-bootstrap";
import { Link } from "react-router-dom";
import "../ScreenCSS/ListHomeStayScreen.css";
import { HomestayContext } from "../../Context/HomestayContext";

const headingArray = ["Name", "Address", "Phone", "City", "Action"];

const ListHomeStayScreen = () => {
  //   useEffect(() => {
  //     fetchAPI();
  //   }, []);

  // const fetchAPI = async () => {
  //   const data = await fetch("https://jsonplaceholder.typicode.com/todos");
  //   const items = await data.json();
  //   setItems(items);
  // };
  const { homestayList } = useContext(HomestayContext);
  if (!homestayList) {
    return <h1>Nothing</h1>;
  }

  console.log(homestayList);
  return (
    <div
      className="flex-grow-1 d-flex flex-column list-hs-container"
    >
      <h1 className="list-hs-title">List Homestay</h1>
      <Table striped hover borderless responsive variant="light">
        <thead>
          <tr style={{ textAlign: "left" }}>
            <th style={{ backgroundColor: "#333333", color: "white" }}>#</th>
            {headingArray.map((item, index) => (
              <th
                style={{ backgroundColor: "#333333", color: "white" }}
                key={index}
              >
                {item}
              </th>
            ))}
          </tr>
        </thead>
        <tbody>
          {homestayList.map((item, index) => (
            <tr key={index} style={{ textAlign: "left" }}>
              <td>{index + 1}</td>
              <td>{item.name}</td>
              <td>{item.address}</td>
              <td>{item.phone}</td>
              <td>{item.city.name}</td>
              <td>
                <ul className="d-flex justify-content-between" style={{listStyle: "none", padding: "0"}}>
                  <li>
                    <Link
                      className="text-decoration-none"
                      to={`/homestays/${item._id}`}
                    >
                      View detail
                    </Link>
                  </li>
                  <li>Delete</li>
                </ul>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </div>
  );
};

export default ListHomeStayScreen;
