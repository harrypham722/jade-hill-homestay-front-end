import React, { useContext, useEffect, useState } from "react";
import FormHomeStayScreen from "./FormHomeStayScreen";
import { HomestayContext } from "../../Context/HomestayContext";
import { CityContext } from "../../Context/CityContext";
import HomestayAPI from "../../API/HomestayAPI/HomestayAPI";

const UpdateHomestayScreen = ({ match }) => {
  const { cityID } = useContext(CityContext);
  const { getParticularHomestay } = HomestayAPI();
  const [homestayItem, setHomestayItem] = useState(undefined);
  const { updateHomestay, homestayList } = useContext(HomestayContext);

  useEffect(() => {
    const fetchData = async () => {
      const result = await getParticularHomestay(match.params.homestayId);
      setHomestayItem(result);
    };
    fetchData();
  }, [match]);

  if (homestayItem === undefined) {
    return <h1>Loading...</h1>;
  }
  const indexSelect = cityID.findIndex((item) => item === homestayItem.city);
  const initial_state = {
    name: homestayItem.name,
    address: homestayItem.address,
    city: homestayItem.city,
    description: homestayItem.description,
    images: homestayItem.images,
    phone: homestayItem.phone
  };

  return (
    <FormHomeStayScreen
      init={initial_state}
      actionSubmit={updateHomestay}
      indexSelect={indexSelect}
      update={{ state: true, data: match.params.homestayId }}
      title="Update Homestay"
      btnSubmitTitle="Update"
    />
  );
};

export default UpdateHomestayScreen;
