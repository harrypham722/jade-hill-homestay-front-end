import React, { useContext } from "react";
import FormHomeStayScreen from "./FormHomeStayScreen";
import { HomestayContext } from "../../Context/HomestayContext";
import { CityContext } from "../../Context/CityContext";

const CreateHomestayScreen = () => {
  const { cityID } = useContext(CityContext);
  const { addHomestay } = useContext(HomestayContext);
  const initial_state = {
    name: "",
    address: "",
    city: cityID[0],
    description: "",
    images: [],
    phone: ""
  };
  return (
    <FormHomeStayScreen
      init={initial_state}
      actionSubmit={addHomestay}
      indexSelect={0}
      update={{ state: false, data: null }}
      title="Create Homestay"
      btnSubmitTitle="Create"
    />
  );
};

export default CreateHomestayScreen;
