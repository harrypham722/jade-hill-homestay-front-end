import React from "react";
import ReactDom from "react-dom";

const HomeScreen = () => {
  return (
    <div className="flex-grow-1">
      <h1>Home</h1>
    </div>
  );
};

export default HomeScreen;
