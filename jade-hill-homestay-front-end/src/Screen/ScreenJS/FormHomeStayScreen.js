import React, { useReducer, useState, useContext } from "react";
import ReactDom from "react-dom";
import { Form, Button } from "react-bootstrap";
import "../ScreenCSS/CreateHomeStayScreen.css";
import { withRouter } from "react-router-dom";
import bg_main from "../../../public/images/bg_main.jpg";
import AddNewModal from "../Component/AddNewModal";
import { CityContext } from "../../Context/CityContext";

const reducer = (state, action) => {
  switch (action.type) {
    case "name_changed":
      return { ...state, name: action.value };
    case "address_changed":
      return { ...state, address: action.value };
    case "phone_changed":
      return { ...state, phone: action.value };
    case "city_changed":
      return { ...state, city: action.value };
    case "description_changed":
      return { ...state, description: action.value };
    case "images_changed":
      return { ...state, images: action.value };
    default:
      return state;
  }
};

const FormHomeStayScreen = ({
  history,
  indexSelect,
  init,
  actionSubmit,
  update,
  title,
  btnSubmitTitle,
}) => {
  const { cityID, addCity } = useContext(CityContext);
  const [formInformation, dispatch] = useReducer(reducer, init);
  const [showModal, setShowModal] = useState(false);

  const handleSubmit = (event) => {
    event.preventDefault();
    const { name, address, description, city, images, phone } = formInformation;
    if (update.state === false) {
      actionSubmit({ name, address, description, city, images, phone });
      history.push("/homestays");
      return;
    }
    if (update.state === true) {
      actionSubmit(
        {
          name,
          address,
          description,
          city,
          images,
          phone,
        },
        update.data
      );
      history.push(`/homestays/${update.data}`);
    }
  };

  const openModal = () => {
    setShowModal(true);
  };

  const closeModal = () => {
    setShowModal(false);
  };
  return (
    <div
      className="flex-grow-1"
      style={{ backgroundColor: "white"}}
    >
      <h1 className="create-homestay-title">{title}</h1>
      <Form
        className="col-10 col-md-8 offset-1 offset-md-2 d-flex flex-column"
        onSubmit={handleSubmit}
      >
        <Form.FloatingLabel label="Name" className="mb-3">
          <Form.Control
            required
            placeholder="name"
            onChange={({ target: { value } }) =>
              dispatch({ type: "name_changed", value: value })
            }
            value={formInformation.name}
          />
        </Form.FloatingLabel>

        <Form.FloatingLabel label="Address" className="mb-3">
          <Form.Control
            required
            placeholder="address"
            onChange={({ target: { value } }) =>
              dispatch({ type: "address_changed", value: value })
            }
            value={formInformation.address}
          />
        </Form.FloatingLabel>

        <Form.FloatingLabel label="Phone" className="mb-3">
          <Form.Control
            required
            placeholder="phone"
            onChange={({ target: { value } }) =>
              dispatch({ type: "phone_changed", value: value })
            }
            value={formInformation.phone}
          />
        </Form.FloatingLabel>

        <div className="d-flex mb-3">
          <Form.Select
            required
            onChange={({ target: { value } }) =>
              dispatch({ type: "city_changed", value: value })
            }
          >
            {cityID.map((item, index) =>
              index === indexSelect ? (
                <option key={index} value={item} seleted="true">
                  {item}
                </option>
              ) : (
                <option key={index} value={item}>
                  {item}
                </option>
              )
            )}
          </Form.Select>
          <Button
            className="col-3 ms-2 bg-secondary create-hs-btn"
            onClick={openModal}
          >
            Add new city
          </Button>
        </div>
        <Form.FloatingLabel label="Description" className="mb-3">
          <Form.Control
            as="textarea"
            placeholder="Description"
            style={{ height: "100px" }}
            required
            onChange={({ target: { value } }) =>
              dispatch({ type: "description_changed", value: value })
            }
            value={formInformation.description}
          />
        </Form.FloatingLabel>
        <Form.Group
          className="mb-3"
          style={{ textAlign: "center", color: "white" }}
        >
          <Form.Label style={{ fontSize: "larger" }}>Upload images</Form.Label>
          <Form.Control
            type="file"
            multiple
            onChange={({ target: { files } }) =>
              dispatch({ type: "images_changed", value: files })
            }
          />
        </Form.Group>
        <Button className="bg-secondary create-hs-btn" type="submit">
          {btnSubmitTitle}
        </Button>
      </Form>
      <AddNewModal
        title="Add new city"
        show={showModal}
        close={closeModal}
        action={addCity}
        input={true}
        actionName="Add"
      />
    </div>
  );
};

export default withRouter(FormHomeStayScreen);
