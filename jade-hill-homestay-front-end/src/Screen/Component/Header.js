import React from "react";
import ReactDom from "react-dom";
import "../ScreenCSS/Header.css";
import sample_avatar from "../../../public/images/sample-avatar.png";
import { FaEllipsisH, FaCaretDown } from "react-icons/fa";

const Header = () => {
  return (
    <div className="header-container">
      <div className="header-second-container">
        <img className="header-avatar" src={`${sample_avatar}`}></img>
        <div className="header-item">
          <FaEllipsisH size={24} color="#eeeeee"/>
        </div>
        <div className="header-item">
          <FaCaretDown size={24} color="#eeeeee"/>
        </div>
      </div>
    </div>
  );
};

export default Header;
