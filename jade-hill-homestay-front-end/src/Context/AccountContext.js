import React, { useReducer, useEffect, useContext } from "react";
import AccountAPI from "../API/AccountAPI/AccountAPI";
import { LoginContext } from "./LoginContext";

export const AccountContext = React.createContext(null);

const reducer = (state, action) => {
  switch (action.type) {
    case "GET_ALL_ACCOUNT":
      return [...action.value];
    default:
      return state;
  }
};

export default ({ children }) => {
  const [accountList, dispatch] = useReducer(reducer, []);
  const { token } = useContext(LoginContext);
  const { getAllAccount, postNewAccount } = AccountAPI();
  useEffect(() => {
    const fetchData = async () => {
      const result = await getAllAccount(token);
      getAccount(result);
    };
    if (token !== null) {
      console.log(token)
      fetchData();
    }
  }, [token]);

  const getAccount = (value) => {
    dispatch({ type: "GET_ALL_ACCOUNT", value: value });
  };

  const addAccount = async (value) => {
    await postNewAccount(value);
    const result = await getAllAccount();
    getAccount(result);
  };

  return (
    <AccountContext.Provider value={{ accountList, addAccount, getAccount }}>
      {children}
    </AccountContext.Provider>
  );
};
