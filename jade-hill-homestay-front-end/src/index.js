import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import { BrowserRouter as Router } from "react-router-dom";
import HomestayProvider from "./Context/HomestayContext";
import CityProvider from "./Context/CityContext";
import AccountContext from "./Context/AccountContext";
import LoginContext from "./Context/LoginContext";

ReactDOM.render(
  <LoginContext>
    <HomestayProvider>
      <CityProvider>
        <AccountContext>
          <Router>
            <App />
          </Router>
        </AccountContext>
      </CityProvider>
    </HomestayProvider>
  </LoginContext>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
